"""
-------------------------------------------------------------------------------
Developed by Fran Quero for reGOSH 2023 Chile. Licensed under GPL-3.0.

This code is designed for visualizing geographical data related to the area we were working 
on during the residency, as well as the samples we took. 

Each data point provides a popup with detailed data and associated photos when clicked. 
The final map is saved as an HTML file to be later on embedded in the wiki.

Use:
1. Ensure required geojson files and associated photos are in the correct directories.
2. Run pip install -r requirements.txt
3. Run the code.
4. Open the generated 'mapa.html' in a web browser to view the interactive map.
-------------------------------------------------------------------------------
"""



# Standard library imports
import os
import glob

# Third-party library imports
import geopandas as gpd
import folium

def get_value_or_default(row, column_name):
    """Retrieve the value from a row. If it's NaN or None, return 'no_data'."""
    value = row[column_name]
    return value if (value == value and value is not None) else 'no_data'

# Load geojson files
HidroLayer = gpd.read_file('HidroLayer.geojson')
datos = gpd.read_file('muestreos.geojson')

# Convert the HidroLayer CRS to EPSG 4326
HidroLayer = HidroLayer.to_crs(epsg=4326)

# Initialize the map centered on the centroid of the HidroLayer
map_center = [HidroLayer.geometry.centroid.iloc[0].y, HidroLayer.geometry.centroid.iloc[0].x]
m = folium.Map(location=map_center, zoom_start=10)

# Add HidroLayer to the map
folium.GeoJson(HidroLayer, name="Hidrological layer").add_to(m)

# Directory containing photos associated with the samples
photo_dir = "photos/"

# Generate markers with popups for each sample in 'datos'
for idx, row in datos.iterrows():
    name = row['id']
    
    # Search for associated photos
    matching_photos = sorted(glob.glob(os.path.join(photo_dir, f"{name}_*.jpg")))
    matching_photos = [photo.replace('\\', '/') for photo in matching_photos]
    
    # Extract specified data for the popup
    data_fields = [
        "lugar", "nombre", "coment", "long_x", "lat_y", "ele", "fecha_hora", 
        "ph", "temp_ph", "condu", "temp_condu", "caudal", "temp", "adn_amb"
    ]
    
    # Organize data into two columns for display
    half_length = len(data_fields) // 2
    column1_content = "<br>".join([f"<strong>{field}</strong>: {get_value_or_default(row, field)}" for field in data_fields[:half_length]])
    column2_content = "<br>".join([f"<strong>{field}</strong>: {get_value_or_default(row, field)}" for field in data_fields[half_length:]])
    
    data_content = (
        f'<div style="width: 300px;">'
        f'<div style="display: inline-block; width: 50%; padding-right: 20px;">{column1_content}</div>'
        f'<div style="display: inline-block; width: 50%;">{column2_content}</div>'
        f'</div>'
    )
    
    # Construct the popup content based on available photos
    if len(matching_photos) == 1:
        photo_path = matching_photos[0]
        popup_content = f'<strong>{name}</strong><br>{data_content}<br><img src="{photo_path}" width="300" align="center">'
    elif len(matching_photos) >= 2:
        photo_path1 = matching_photos[0]
        # photo_path2 = matching_photos[1]  # Commented out as it's not used
        popup_content = f'<strong>{name}</strong><br>{data_content}<br><img src="{photo_path1}" width="300" align="center">'
    else:
        popup_content = f'<strong>{name}</strong><br>{data_content}'
    
    # Add the marker to the map
    folium.Marker(
        location=[row['geometry'].y, row['geometry'].x],
        popup=popup_content
    ).add_to(m)

# Add a control to toggle layers
folium.LayerControl().add_to(m)

# Save the generated map to an HTML file
m.save('mapa.html')