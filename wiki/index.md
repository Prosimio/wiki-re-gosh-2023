# reGOSH 2023 Estación Patagonia, Valle de los Exploradores, región de Aysén, Chile

Esta Wiki centraliza la documentación de los proyectos y desarrollos llevados a cabo durante esta residencia.


![Glaciar Exploradores](https://gitlab.com/luherbert/wiki-regosh-2023/-/raw/main/imagenes/portada/nosotres.jpg)
> Foto: Nico Méndez